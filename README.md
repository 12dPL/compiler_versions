# 12d Programming Language Compiler Versions

With new releases of 12d Model software, the corresponding compiler for the 
12d Programming Language (12dPL) can also change. Typically, changes to the 
compiler are made to add new library calls, new instructions or fixes. 
Previously, these changes tended to only occur when a new major version of 12d
Model was released (e.g. V12 or V14).  However, more recently (largely since 
V14), more frequent updates have been made. 

## Before V14C2g 

When running the 12dPL compiler, cc4d.exe, the Version information contained 3 
numbers.
`cc4d.exe`

```
4D Model Programming Language Compiler
--------------------------------------

Version: 801 / 171 / 3209
```
These 3 numbers correspond respectively to:
1.  Macro Version
2.  Last Opcode
3.  Last Library Code

So, in the above example, the Macro Version = 801, the Last Opcode = 171 and
the Last Library Code = 3209. The Last Library Code is the most useful of these
to macro authors, since it corresponds to the function ID listed in the 
prototypes file.

Opcode refers to more specific language features such as object/variable types
(e.g. Integer64, Guid), operators (e.g. +, -, *, /), keywords and the like. 
These are likely to change less frequently, but are more significant in their 
impact.

## V14C2f onwards

From V14C2g onwards, the Version information is still present, however, 
additional compiler variables are set also containing this information.
These variables are:
1. MACRO_VERSION_4D
2. MACRO_LAST_OPCODE_4D
3. MACRO_LAST_LIBRARY_CODE_4D

The values of these variables will be integers corresponding to the version
numbers indicated above. These variables can then be used in your 12dPL code 
and will be parsed and interpreted by the 12dPL preprocessor.  This is 
particularly useful when targeting multiple versions of 12d Model with 
varying support for certain 12dPL function calls.

For example:
```
Integer Set_allow_holes(Polygon_Box select,Integer allow); // ID = 3801
```
This library call has an ID = 3801. This means that it can only be compiled
with and used in a compiler with support for this call.
i.e. a MACRO_LAST_LIBRARY_CODE_4D >= 3801

If you have a macro or common routines in a library, you can use these compiler
variables to include/exclude certain functionality or switch between different
implementations without keeping multiple copies.
```
#if MACRO_LAST_LIBRARY_CODE_4D >= 3801
// We can only use this call in V14C2g or later (see table below)
Set_allow_holes(my_polygon_box, TRUE);    // ID = 3801
#else
// Otherwise, we can't use this call in earlier versions
#endif
```

## Known Values

Below are the known values for previous version of 12d Model. 

| 12d Model Version | MACRO_VERSION_4D | MACRO_LAST_OPCODE | MACRO_LAST_LIBRARY_CODE_4D |
|:-----------------:|:----------------:| -----------------:| --------------------------:|
| V8  | 801 | 109 | 2240 |
| V9  | 801 | 160 | 2645 |
| V10 | ??? | ??? | ???? |
| V11 | 801 | 171 | 3102 |
| V12 | 801 | 171 | 3209 |
| V14C2c | 801 | 218 | 3737 |
| V14C2d | 801 | 218 | 3764 |
| V14C2f | 801 | 218 | 3794 |
| V14C2g | 801 | 218 | 3812 |
| V14C2h | 801 | 218 | 3816 |
| V14C2i | 801 | 218 | 3862 |
| V14C2k | 801 | 218 | 3862 |
| V15C1a | 801 | 218 | 7675 |

If you have any more details or information on missing values, feel free to 
get in contact or make a merge request.





